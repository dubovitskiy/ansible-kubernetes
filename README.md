# Kubernetes cluster sandbox

`WARNING`: this playbook can be ran just only once and
only on clean machine due to some issues (see below in TODO section).

`WARNING1`: this playbook doesn't guarantee anything. This
project is used to learn how write ansible playbooks.

`WARNING2`: there are probably no best practices used.
This project has been started from scratch as an ansible
practice. Kubernetes has been deployed using only official
guides and almost no hints was used but Google.

### How to get the cluster assembled

1. Create 3 or more virtual machines with CentOS7 installed.

2. Add your public key to each machine from management
host

3. Create your inventory file.

Inventory file has to contain the following groups:

* `kubenodes` — all the nodes in your cluster
* `kubernetes-master-node` has to contain only master node
* `kubernetes-minions` has to contain worker nodes

Example:

```
[kubenodes]
kubernetes-master ansible_host=192.168.1.85 ansible_user=root
kubernetes-minion-1 ansible_host=192.168.1.89 ansible_user=root
kubernetes-minion-2 ansible_host=192.168.1.88 ansible_user=root

[kubernetes-master-node]
kubernetes-master ansible_host=192.168.1.85 ansible_user=root

[kubernetes-minions]
kubernetes-minion-1 ansible_host=192.168.1.89 ansible_user=root
kubernetes-minion-2 ansible_host=192.168.1.88 ansible_user=root

```

4. Create snapshots of your vms (to stay patient).

5. Run installation playbook

```sh
$ ansible-playbook -i ./inventory playbook.yml
```

Links:

[Cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

[Validation](https://github.com/kubernetes/kubernetes/blob/master/cluster/validate-cluster.sh)

##### TODO

* Validate cluster health in order not to execute `init` if cluster has
already exists.
* Use `become` parameter instead of executing commands by root user.
* Get rid of `kube-cluster` and `kube-node` roles and combine their
tasks with other existing roles.

### Installation source docs

https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

https://kubernetes.io/docs/setup/independent/install-kubeadm/

https://github.com/kubernetes/kubernetes/tree/master/cluster